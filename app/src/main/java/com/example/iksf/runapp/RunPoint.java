package com.example.iksf.runapp;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;


public class RunPoint implements Comparable<RunPoint> {
    public GeoPoint point;
    public Timestamp time;

    public RunPoint(GeoPoint p, Timestamp t) {
        point = p;
        time = t;
    }

    public int compareTo(RunPoint other) {
        return time.compareTo(other.time);
    }
}
