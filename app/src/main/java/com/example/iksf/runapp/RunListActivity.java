package com.example.iksf.runapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class RunListActivity extends BaseActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_list);
        mDb.collection("run_metadata").whereEqualTo("user_id", mUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    ArrayList<RunData> runs = new ArrayList<>();
                    for (DocumentSnapshot doc : task.getResult()) {
                        double avg_speed = doc.getDouble("avg_speed");
                        double cal = doc.getDouble("cal");
                        double distance = doc.getDouble("distance");
                        String run_id = doc.getString("run_id");
                        Date date_time = doc.getDate("timestamp");
                        double time = doc.getDouble("time");
                        RunData r = new RunData(avg_speed, cal, distance, time, run_id, date_time);
                        runs.add(r);
                    }
                    for (RunData r : runs) {
                        r.populatePointsList(mDb);
                    }
                    Collections.sort(runs);
                    layoutStuff(runs);
                }
            }
        });
    }

    private void layoutStuff(ArrayList<RunData> myDataSet) {
        mRecyclerView = findViewById(R.id.runListActivityRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RunListAdapter(this, myDataSet);
        mRecyclerView.setAdapter(mAdapter);
    }
}
