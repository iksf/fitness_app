package com.example.iksf.runapp;
/*
todo
this needs serious work
 */

public class RunnerFitness {
    private final double KG_PER_LB = 0.453592;
    private final double KM_PER_MILE = 1.60934;
    private short weight;
    private short height;
    private short age;
    private short calPerKilometre;

    RunnerFitness(short weight, short height, short age) {
        this.weight = weight;
        this.height = height;
        this.age = age;


        if (weight < 120 * KG_PER_LB) {
            this.calPerKilometre = (short) (91 * KM_PER_MILE);
        } else if (weight < 140 * KG_PER_LB) {
            this.calPerKilometre = (short) (105 * KM_PER_MILE);
        } else if (weight < 160 * KG_PER_LB) {
            this.calPerKilometre = (short) (121 * KM_PER_MILE);
        } else if (weight < 180 * KG_PER_LB) {
            this.calPerKilometre = (short) (136 * KM_PER_MILE);
        } else {
            this.calPerKilometre = (short) (200 * KM_PER_MILE);
        }
    }

    public RunnerFitness(int weight) {
        this((short) weight, (short) 0, (short) 0);
    }

    public long caloriesBurnt(double d, double seconds) {
        /*
        todo
        some calculation involving calPerKilometre (which is just lies i made up anyway),
        the distance and the time -> the speed -> the calories burnt
         */
        return (long) (d / 1000 * this.calPerKilometre);
    }
}
