package com.example.iksf.runapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class RunListAdapter extends RecyclerView.Adapter<RunListAdapter.ViewHolder> {
    private static Context context;
    private ArrayList<RunData> mDataset;


    public RunListAdapter(Context ctx, ArrayList<RunData> myDataset) {
        mDataset = myDataset;
        context = ctx;
    }

    @Override
    public RunListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View k = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.run_list_element, parent, false);
        ViewHolder vh = new ViewHolder(k);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Date date = mDataset.get(position).date_time;
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(date); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
        Date newTime = cal.getTime(); // returns new date object, one hour in the future
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
        final String strDate = dateFormat.format(newTime);

        holder.mDateLabel.setText(strDate);
        holder.mElementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                RunData r = mDataset.get(position);
                intent.putExtra("speed", String.valueOf(r.avg_speed));
                intent.putExtra("cal", String.valueOf(r.cal));
                intent.putExtra("distance", String.valueOf(r.distance));
                intent.putExtra("date", strDate);
                intent.putExtra("duration", String.valueOf(r.duration));
                intent.putExtra("latlngs", r.latLngs());
                Log.d("MyLogger", "latlngs len: " + r.latLngs().size() + " run id: " + r.run_id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDateLabel;
        public Button mElementButton;

        public ViewHolder(View v) {
            super(v);
            mDateLabel = v.findViewById(R.id.runListElementDate);
            mElementButton = v.findViewById(R.id.runListElementViewEntryButton);
        }
    }
}
