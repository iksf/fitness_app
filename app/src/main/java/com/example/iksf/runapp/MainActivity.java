package com.example.iksf.runapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends BaseActivity {

    private TextView mEmail;
    private TextView mPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEmail = findViewById(R.id.mainEmail);
        mPassword = findViewById(R.id.mainPassword);

    }

    @Override
    public void onStart() {
        super.onStart();
        mEmail.setText(null);
        mPassword.setText(null);

        if (mUser == null) {
            Log.d("MyLogger", "Not logged in");
        } else {
            Log.d("MyLogger", "Logged in with " + mUser);
            checkForHasDetails();
        }
    }

    private void checkForHasDetails() {
        mDb.collection("user_details").whereEqualTo("user_id", mUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    if (!task.getResult().isEmpty()) {
                        sendToWelcomeActivity();
                    } else {
                        Log.d("MyLoggerQuery", "No data for this user, getting it...");
                        sendToGetDetails();
                    }
                } else {
                    Log.d("MyLoggerQuery", "Some failure, sending for details");
                    /*

                    todo
                    I dont know what would cause a successful query that returned a failure, maybe an API issue?

                     */
                    sendToGetDetails();
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MyLoggerQuery", "Some onFailure failure, sending for details");
                        /*
                        todo
                        This is probably due to network errors or something and should inform user appropriately
                         */

                        sendToGetDetails();

                    }
                });

    }

    public void mainSignInClicked(View v) {
        if (!(mEmail.getText().toString().isEmpty()) && !(mPassword.getText().toString().isEmpty())) {
            signIn(mEmail.getText().toString(), mPassword.getText().toString());
        } else {
            loginError();
        }
    }

    public void mainSignUpClicked(View v) {
        createAccount(mEmail.getText().toString(), mPassword.getText().toString());
    }

    private void createAccount(String email, String password) {
        if (validateInput(email, password)) {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d("MyLogger", "Success making user");
                            /*
                            todo
                            some success prompt?
                             */
                                sendToGetDetails();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            accountCreationError();
                        }
                    });
        } else {
            accountCreationError();
        }
    }

    private Boolean validateInput(String email, String password) {
        if (email != null && password != null) {
            if (email.contains("@") && password.length() >= 6) {
                return true;
            }
        }
        Log.d("MyLogger", "Validation failed");
        return false;

    }

    private void loginError() {
        errorPrompt("Login failed");
    }

    private void accountCreationError() {

        errorPrompt("Account creation failed");
    }

    private void errorPrompt(String error) {
        Toast toast = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }


    private void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("MyLogger", "Login successful");
                            sendToWelcomeActivity();
                        } else {
                            loginError();
                            Log.d("MyLogger", "Login failed");
                            /*
                            todo
                            important: inform user of invalid details
                             */
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                           /*
                        todo
                        This is probably due to network errors or something and should inform user appropriately
                         */

                        loginError();
                        Log.d("MyLogger", "Login failed");
                    }
                });
    }

}
