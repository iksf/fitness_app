package com.example.iksf.runapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class BaseActivity extends Activity {

    protected FirebaseAuth mAuth;
    protected FirebaseFirestore mDb;
    protected FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFireBase();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mUser != null) {
            Log.d("MyLogger", "Current user: " + mUser.getUid());
        }
    }

    protected void initFireBase() {
        mAuth = FirebaseAuth.getInstance();
        mDb = FirebaseFirestore.getInstance();
        mUser = mAuth.getCurrentUser();
    }

    protected void sendToGetDetails() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        startActivity(intent);
    }

    protected void sendToRunListActivity() {
        Intent intent = new Intent(this, RunListActivity.class);
        startActivity(intent);
    }

    protected void sendToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    protected void sendToWelcomeActivity() {

        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
    }


    protected void sendToNewRunActivity() {

        Intent intent = new Intent(this, NewRunActivity.class);
        startActivity(intent);
    }

    protected void signOut() {
        mAuth.signOut();
        sendToMainActivity();
    }

}
